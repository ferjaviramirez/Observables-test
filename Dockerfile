FROM node:14-alpine as build-step

RUN mkdir -p /app

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . /app

RUN npm run build --prod

#STAGE FINAL

FROM nginx:1.21.0-alpine

COPY --from=build-step /app/dist/observables /usr/share/nginx/html